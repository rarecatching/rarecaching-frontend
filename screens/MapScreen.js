import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button, Alert, TextInput, TouchableOpacity, Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import axios from 'axios';

// Définition du composant MapScreen qui accepte les props route
const MapScreen = ({ route }) => {
  // Récupération du role de l'utilisateur à partir des params de la route
  const { userRole } = route.params;
  
  // Initialisation des états pour gérer les caches, la région de la carte, et les inputs pour ajouter un nouveau cache
  const [caches, setCaches] = useState([]);
  const [region, setRegion] = useState({
    latitude: 44.8416, // Latitude initiale
    longitude: -0.580814, // Longitude initiale
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  // Fonctions pour ajuster le zoom de la carte
  const zoomIn = () => {
    setRegion(prevState => ({
     ...prevState,
      latitudeDelta: prevState.latitudeDelta * 0.5, // Réduction du delta pour zoomer dedans
      longitudeDelta: prevState.longitudeDelta * 0.5,
    }));
  };

  const zoomOut = () => {
    setRegion(prevState => ({
     ...prevState,
      latitudeDelta: prevState.latitudeDelta * 2, // Augmentation du delta pour zoomer dehors
      longitudeDelta: prevState.longitudeDelta * 2,
    }));
  };

  // État pour gérer l'affichage des inputs d'ajout de cache
  const [showInputs, setShowInputs] = useState(false);

  // États pour gérer les valeurs des inputs de nom et description du cache à ajouter
  const [newCacheName, setNewCacheName] = useState('');
  const [newCacheDescription, setNewCacheDescription] = useState('');

  // Effet secondaire pour charger les caches depuis l'API au montage du composant
  useEffect(() => {
    fetchCaches();
  }, []);

  // Fonction asynchrone pour récupérer les caches depuis l'API
  const fetchCaches = async () => {
    try {
      const response = await axios.get('http://localhost:3000/caches'); // Demande GET pour récupérer les caches
      const parsedData = response.data.map(cache => ({
       ...cache,
        latitude: parseFloat(cache.latitude),
        longitude: parseFloat(cache.longitude),
      }));
      setCaches(parsedData);
    } catch (error) {
      console.error(error);
    }
  };

  // Fonction asynchrone pour ajouter un nouveau cache
  const handleAddCache = async () => {
    if (!newCacheName ||!newCacheDescription) {
      Alert.alert("Ajout de Cache", "Veuillez remplir tous les champs.");
      return;
    }

    try {
      const response = await axios.post('http://localhost:3000/caches', { // Demande POST pour ajouter un cache
        name: newCacheName,
        description: newCacheDescription,
        latitude: region.latitude.toString(),
        longitude: region.longitude.toString(),
      });
      Alert.alert("Succès", "Cache ajouté avec succès!");
      fetchCaches(); // Actualisation de la liste des caches après l'ajout
    } catch (error) {
      console.error(error);
      Alert.alert("Erreur", "Échec de l'ajout du cache.");
    }
  };

  // Rendu du composant
  return (
    <View style={styles.container}>
      {!showInputs? (
        <>
          <MapView
            style={styles.map}
            region={region}
            onRegionChangeComplete={setRegion} // Mise à jour de la région lorsque l'utilisateur change la vue de la carte
          >
            {caches.map((cache, index) => (
              <Marker
                key={cache.id}
                coordinate={{ latitude: cache.latitude, longitude: cache.longitude }}
                title={cache.name}
                description={cache.description}
              />
            ))}
          </MapView>
          {/* Boutons pour ajuster le zoom */}
          <TouchableOpacity style={styles.zoomButton} onPress={zoomIn}>
            <Text style={styles.buttonText}>+</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.zoomButton, styles.zoomOutButton]} onPress={zoomOut}>
            <Text style={styles.buttonText}>-</Text>
          </TouchableOpacity>
          {userRole === 'admin' && (
            <TouchableOpacity style={styles.addButton} onPress={() => setShowInputs(true)}>
              <Text style={styles.addButtonText}>Ajouter un Cache</Text>
            </TouchableOpacity>
          )}
        </>
      ) : (
        <>
          <TextInput
            placeholder="Nom du Cache"
            value={newCacheName}
            onChangeText={setNewCacheName}
            style={styles.input}
          />
          <TextInput
            placeholder="Description du Cache"
            value={newCacheDescription}
            onChangeText={setNewCacheDescription}
            style={styles.input}
          />
          <Button title="Soumettre" onPress={handleAddCache} />
          <Button title="Annuler" onPress={() => setShowInputs(false)} />
        </>
      )}
    </View>
  );
};

// Styles CSS pour l'écran de carte
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: 'gray',
    width: '80%',
    padding: 10,
    marginBottom: 10,
  },
  map: {
    width: '100%',
    height: '100%',
  },
  zoomButton: {
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 20,
    padding: 10,
    margin: 10,
    zIndex: 1,
    right: 10,
    bottom: 90,
  },
  zoomOutButton: {
    bottom: 30,
  },
  addButton: {
    position: 'absolute',
    bottom: 30,
    left: 10,
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  addButtonText: {
    color: 'white',
  },
  buttonText: {
    fontSize: 20,
  },
});

export default MapScreen;
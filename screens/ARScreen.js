import React, { useRef, useEffect, useState } from 'react';
import { View } from 'react-native';
import { GLView } from 'expo-gl';
import { Renderer } from 'expo-three';
import * as Three from 'three';
import axios from 'axios';

const ARScreen = () => {/*
  const [caches, setCaches] = useState([]);
  let renderer;
  const glViewRef = useRef(null);

  useEffect(() => {
    axios.get('http://localhost:3000/caches')
      .then(response => setCaches(response.data))
      .catch(error => console.error(error));

    return () => {
      if (renderer) {
        renderer.dispose();
      }
    };
  }, []);

  const onContextCreate = async (gl) => {
    const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
    renderer = new Renderer({ gl });
    renderer.setSize(width, height);

    const scene = new Three.Scene();
    const camera = new AR.Camera(width, height, 0.01, 1000);
    camera.position.set(0, 0, 0);

    const light = new Three.DirectionalLight(0xffffff, 1);
    light.position.set(1, 1, 1).normalize();
    scene.add(light);

    caches.forEach(cache => {
      const geometry = new Three.BoxGeometry(0.1, 0.1, 0.1);
      const material = new Three.MeshPhongMaterial({ color: 0xff0000 });
      const cube = new Three.Mesh(geometry, material);
      cube.position.set(cache.latitude, cache.longitude, -0.5);
      scene.add(cube);
    });

    const render = () => {
      requestAnimationFrame(render);
      renderer.render(scene, camera);
      gl.endFrameEXP();
    };
    render();
  };

  return (
    <View style={{ flex: 1 }}>
      <GLView
        style={{ flex: 1 }}
        onContextCreate={onContextCreate}
        ref={glViewRef}
      />
    </View>
  );*/
};

export default ARScreen;

import React from 'react';
import { View, Text, Button } from 'react-native';

const CacheDetailsScreen = ({ route }) => {
  const { id } = route.params;
  
  // Initialisation des données du cache avec l'id récupéré
  const cache = { id, name: 'Cache Details', description: 'Detailed Description' };

  // Rendu du composant
  return (
    <View> {}
      <Text>{cache.name}</Text> {}
      <Text>{cache.description}</Text> {}
      <Button title="Retour" onPress={() => navigation.goBack()} /> {}
    </View>
  );
};

export default CacheDetailsScreen;
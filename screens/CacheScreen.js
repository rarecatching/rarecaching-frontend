import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import axios from 'axios';
import GlobalStyles from '../styles/GlobalStyles';

const CacheScreen = () => {
  // Initialisation de l'état pour stocker les données des caches
  const [caches, setCaches] = useState([]);

  // Effet secondaire pour charger les données des caches depuis l'API au montage du composant
  useEffect(() => {
    axios.get('http://localhost:3000/caches') // Demande GET pour récupérer les données des caches
     .then(response => setCaches(response.data)) // Mise à jour de l'état avec les données reçues
     .catch(error => console.error(error)); // Gestion des erreurs
  }, []);

  // Rendu du composant
  return (
    <View style={GlobalStyles.container}>
      <Text style={GlobalStyles.text}>Disponibles Caches</Text> {/* Titre de l'écran */}
      {caches.map((cache, index) => ( // Boucle sur chaque élément du tableau caches
        <View key={index} onPress={() => navigation.navigate('CacheDetailsScreen', { id: cache.id })}> {/* Navigation vers les détails d'un cache */}
          <Text>{cache.name}</Text> {/* Affichage du nom du cache */}
          <Text>{cache.description}</Text> {/* Affichage de la description du cache */}
        </View>
      ))}
    </View>
  );
};

export default CacheScreen;
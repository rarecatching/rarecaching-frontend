import React from 'react';
import { View, Text, Button } from 'react-native';

// Définition du composant HomeScreen qui accepte les props navigation
const HomeScreen = ({ navigation }) => {
  // Rendu du composant
  return (
    <View> {/* Conteneur principal pour organiser les enfants */}
      <Text>Bienvenue sur RareCaching</Text> {/* Texte de bienvenue */}
      <Button title="Aller vers le Cache" onPress={() => navigation.navigate('Cache')} /> {/* Bouton pour naviguer vers l'écran Cache */}
      <Button title="Aller vers la Carte" onPress={() => navigation.navigate('Map')} /> {/* Bouton pour naviguer vers l'écran Map */}
    </View>
  );
};

export default HomeScreen;
import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Button, Alert } from 'react-native';
import axios from 'axios';

// Définition du composant LoginScreen qui reçoit les props navigation et route
const LoginScreen = ({ navigation, route }) => {
  // Utilisation de l'état local pour gérer les valeurs de username et password
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  // Récupération de la fonction setUserRole passée via les params de la route
  const { setUserRole } = route.params;

  // Fonction asynchrone pour gérer la connexion
  const handleLogin = async () => {
    try {
      // Appel API pour récupérer tous les utilisateurs
      const response = await axios.get('http://localhost:3000/users');
      console.log("Statut de la réponse Axios :", response.status);
      console.log("En-têtes de la réponse Axios :", response.headers);
      console.log("Données de la réponse Axios :", response.data);
      // Recherche de l'utilisateur correspondant dans la liste des utilisateurs
      const user = response.data.find(user => user.username === username && user.password === password);
      if (user) {
        // Si utilisateur trouvé, définir son rôle et naviguer vers l'écran MapScreen
        setUserRole(user.role);
        navigation.navigate('MapScreen', { userRole: user.role });
      } else {
        // Si aucun utilisateur n'est trouvé, afficher une alerte d'échec de connexion
        Alert.alert('Échec de la connexion', 'Nom d\'utilisateur ou mot de passe invalide');
      }
    } catch (error) {
      console.error("Échec de la requête Axios :", error.message);
      Alert.alert('Erreur', 'Impossible de se connecter');
    }
  };

  // Retourne le JSX pour l'affichage de l'écran de connexion
  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Nom d'utilisateur"
        value={username}
        onChangeText={setUsername}
        style={styles.input}
      />
      <TextInput
        placeholder="Mot de passe"
        secureTextEntry // Masque le texte saisi
        value={password}
        onChangeText={setPassword}
        style={styles.input}
      />
      <Button title="Se connecter" onPress={handleLogin} /> {/* Bouton pour valider la connexion */}
    </View>
  );
};

// Styles CSS pour l'écran de connexion
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: 'gray',
    width: '80%',
    padding: 10,
    marginBottom: 10,
  },
});

export default LoginScreen;
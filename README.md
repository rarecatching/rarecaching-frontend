
**RareCaching**

**Contexte**

RareCaching est une application mobile développée avec React Native destinée à la communauté des géocacheurs. Elle permet aux utilisateurs de découvrir des caches cachés à travers le monde, de visualiser leur emplacement sur une carte, d'en savoir plus sur ces caches et même d'ajouter leurs propres découvertes.

**Structure du Projet**

Le projet est structuré en plusieurs fichiers clés et dossiers principaux :

App.js : Point d'entrée de l'application qui initialise le conteneur de navigation.

screens/ : Contient tous les écrans de l'application :

HomeScreen.js : Écran d'accueil présentant deux boutons pour naviguer vers les écrans Cache et Map.

CacheScreen.js : Liste les caches disponibles.

MapScreen.js : Affiche une carte interactive avec des marqueurs pour chaque cache et permet aux utilisateurs d'ajouter de nouveaux caches.

CacheDetailsScreen.js : Détails d'un cache sélectionné.

LoginScreen.js : Permet aux utilisateurs de se connecter à leur compte.

navigation/ : Gère la navigation entre les différents écrans de l'application.

components/ : Contiendra tous les composants réutilisables à travers l'application.

**Fonctionnalités Clés**

Authentification : Les utilisateurs peuvent se connecter via l'écran de connexion pour accéder aux fonctionnalités avancées comme l'ajout de caches.

Visualisation des Caches : Les caches sont affichés sous forme de liste et sur une carte interactive.

Ajout de Caches : Les administrateurs ont la possibilité d'ajouter de nouveaux caches directement depuis l'écran de la carte.

Navigation : Une navigation fluide est mise en place pour passer facilement d'un écran à l'autre.

L'application RareCaching repose sur une architecture backend RESTful pour gérer les données des caches et les opérations CRUD (Create, Read, Update, Delete). 

Le backend est construit avec Node.js et Express, et utilise MongoDB comme base de données pour stocker les informations des caches et des utilisateurs. Il expose des endpoints API pour interagir avec l'application frontend.


**Mise en place du projet**

Pour mettre en place le projet RareCaching sur votre environnement local, suivez ces étapes :

**Prérequis**

Avant de commencer, assurez-vous d'avoir installé Node.js, npm/yarn, et React Native CLI sur votre machine.

**Installation**

Clonez le dépôt :

   git clone https://gitlab.com/rarecatching
Installez les dépendances :

   npm install
   
Assurez-vous que vous avez configuré votre environnement de développement React Native.

Pour exécuter l'application sur un émulateur ou un appareil physique, utilisez l'une des commandes suivantes dans le terminal à partir du dossier racine du projet :

Pour iOS :

     npx react-native run-ios
     ```
Pour Android :

     npx react-native run-android
     ```


**Outils Nécessaires**

Pour iOS :

Xcode : Environnement de développement intégré (IDE) pour macOS contenant un ensemble d'instruments de développement logiciels pour développer des applications pour macOS, iOS... Xcode est requis pour compiler et exécuter l'application sur un simulateur iOS ou un appareil physique.

Pour Android :

Android Studio : IDE pour le développement d'applications Android qui fournit l'émulateur Android et les outils nécessaires pour tester l'application sur un appareil virtuel ou physique.


**Lancer le Projet avec Expo**

Installation d'Expo CLI :

   npm install -g expo-cli

Démarrage du projet : À la racine du projet frontend, exécutez :

   expo start
   
Cela ouvrira une fenêtre dans votre navigateur avec un QR code que vous pouvez scanner avec l'application Expo Go sur votre appareil mobile pour démarrer l'application.
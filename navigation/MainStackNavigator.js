import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import CacheScreen from '../screens/CacheScreen';
import MapScreen from '../screens/MapScreen';
import CacheDetailsScreen from '../screens/CacheDetailsScreen';
import LoginScreen from '../screens/LoginScreen';

const Stack = createStackNavigator();

const MainStackNavigator = () => {
  const [userRole, setUserRole] = useState(null);
  return (
 
      <Stack.Navigator>
        {userRole? (
          <Stack.Screen name="Map" component={MapScreen} initialParams={{ userRole }} />
        ) : (
          <Stack.Screen name="Login" component={LoginScreen} initialParams={{ setUserRole }} />
        )}
      </Stack.Navigator>

 
  );
};

export default MainStackNavigator;
